#!/usr/bin/env python3
"""
ContentPostApp class
La clase permite la actualización del contenido mediante peticiones HTTP POST.
Cuando se reciba un GET pidiendo cualquier recurso, se buscará en el diccionario de contenidos,
y si existe, se servirá. En cualquier caso (exista o no exista el contenido en cuestión)
se servirá en la misma página un formulario que permitirá actualizar el contenido del diccionario
(o crear una nueva entrada, si no existía) mediante un POST.
"""
from urllib import parse

import webapp

# Definición de formularios HTML para responder a las peticiones del navegador.
FORM = """
    <hr>
    <form action="/" method="post">
      <div>
        <label>Resource: </label>
        <input type="text" name="resource" required>
      </div>
      <div>
        <label>Content: </label>
        <textarea name="content" rows="5" cols="33" required></textarea>
      </div>
      <div>
        <input type="submit" value="Submit">
      </div>
    </form>
"""

PAGE = """
<!DOCTYPE html>
<html lang="en">
  <body>
    <div>
      <p>Content for {resource}:</p> {content}
    </div>
    <div>
      {form}
    </div>
  </body>
</html>
"""

PAGE_NOT_FOUND = """
<!DOCTYPE html>
<html lang="en">
  <body>
    <div>
      <p>Resource not found: {resource}.</p>
    </div>
    <div>
      {form}
    </div>
  </body>
</html>
"""

PAGE_NOT_ALLOWED = """
<!DOCTYPE html>
<html lang="en">
  <body>
    <p>Method not allowed: {method}.</p>
  </body>
</html>
"""

PAGE_UNPROCESABLE = """
<!DOCTYPE html>
<html lang="en">
  <body>
    <p>Unprocesable POST: {body}.</p>
  </body>
</html>
"""

class ContentApp(webapp.webApp): #La clase se inicializa con un diccionario vacío (contents)

    contents = {'Hola': '¡Hola mundo!', 'Adios': 'Adios mundo cruel'}

    def parse (self, request):
        """Analiza la solicitud HTTP para extraer el método y el recurso.
        También recoge el cuerpo del mensaje si está presente."""

        data = {}
        body_start = request.find('\r\n\r\n')
        if body_start == -1:
            data['body'] = None
        else:
            data['body'] = request[body_start+4:]
        parts = request.split(' ', 2)
        data['method'] = parts[0]
        data['resource'] = parts[1]
        return (data)

    def process (self, data):
        """Procesa la solicitud HTTP y determina qué acción tomar
        en función del método (GET, PUT, POST)"""

        if data['method'] == 'GET':
            code, page = self.get(data['resource'])
        elif data['method'] == 'PUT':
            code, page = self.put(data['resource'], data['body'])
        elif data['method'] == 'POST':
            code, page = self.post(data['resource'], data['body'])
        else:
            code, page = "405 Method not allowed",\
                         PAGE_NOT_ALLOWED.format(method=data['method'])
        return (code, page)

    def get(self, resource):
        """Maneja solicitudes GET. Si el recurso existe en el diccionario de contenidos,
        devuelve una página con el contenido y un formulario para actualizarlo.
        Si no existe, devuelve una página de error."""

        if resource in self.contents:
            content = self.contents[resource]
            page = PAGE.format(content=content, resource=resource, form=FORM)
            code = "200 OK"
        else:
            page = PAGE_NOT_FOUND.format(resource=resource, form=FORM)
            code = "404 Resource Not Found"
        return code, page

    def put(self, resource, body):
        """Maneja solicitudes PUT para actualizar el contenido del recurso.
        Almacena el nuevo contenido y devuelve una página
        con el contenido actualizado y un formulario"""

        self.contents[resource] = body
        page = PAGE.format(content=body, resource=resource, form=FORM)
        code = "200 OK"
        print("Contenido del diccionario actualizado:", self.contents)
        return code, page

    def post(self, resource, body):
        """Maneja solicitudes POST. Si el recurso es la raíz ('/'),
        actualiza el contenido según los campos del formulario y devuelve una página
        con el contenido actualizado y un formulario. Si el recurso no es la raíz,
        devuelve una página de error"""

        fields = parse.parse_qs(body)
        print("El cliente ha introducido:", fields)
        if (resource == '/'):
            if ('resource' in fields) and ('content' in fields):
                resource = fields['resource'][0]
                content = fields['content'][0]
                self.contents[resource] = content
                page = PAGE.format(content=content, resource=resource, form=FORM)
                code = "200 OK"
                print("Contenido del diccionario actualizado:", self.contents)
            else:
                page = PAGE_UNPROCESABLE.format(body=body)
                code = "422 Unprocessable Entity"
        else:
            code = "405 Method not allowed"
            page = PAGE_NOT_ALLOWED.format(method='POST')
        return code, page


if __name__ == "__main__":
    webApp = ContentApp ("localhost", 1234)


